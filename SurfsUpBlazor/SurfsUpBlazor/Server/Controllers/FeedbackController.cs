﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SurfsUpBlazor.Server.Data;
using SurfsUpBlazor.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SurfsUpBlazor.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public FeedbackController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Feedback> Get()
        {
            return _context.Feedback.ToList();
        }

        [HttpPost]
        //[Route("api/Feedback")]
        //[Route("api/Feedback/{id}")]
        public async Task<HttpResponseMessage> Post(Feedback feedback) //HttpResponseMessage result
        {
            _context.Add(feedback);
            await _context.SaveChangesAsync();
            return new HttpResponseMessage(HttpStatusCode.Created);
        }
        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<HttpResponseMessage> Delete(int id) //HttpResponseMessage result
        {
            //Feedback feedback = await _context.Feedback.FindAsync(id);
            Feedback feedback = new Feedback();
            feedback.ID = id;
            _context.Attach(feedback);
            _context.Remove(feedback);
            await _context.SaveChangesAsync();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }



    }
}
